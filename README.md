Thanks for taking the time to work on this take-home challenge! We're excited to see what you put together.

—Team Evergreen


# Process

 * Clone this repository and commit your work to it.
 * Document any requirements or assumptions about your code with inline comments
 * Create a new file, `README-YourName.md`, add it to the root directory, and provide any additional commentary there
 * To submit, zip the repository and email it to us. We will confirm receipt when we get it — if we don't, please ask again!
 * Any questions, email us!


## Evaluation

Aim for basics; skip the sticky parts. You are not being evaluated on the number of lines of code you write, but on the quality and economy of what you write to satisfy the challenge.

Key criteria:

 * Code completeness
 * Style
 * Economy
 * It runs!

When you submit your project, we will review and schedule a 45-minute call to go over your work. On that call, be prepared to talk about

 * Design decisions
 * Trade-offs
 * Possible places to expand
 * Cool things you're proud of


# The Task

Create a basic "Call Log" app to track notes you'd take while talking to interviewers. We need a backend to store and serve notes, and we need a frontend for users to view and make notes.

The user is a job applicant who is engaged with multiple companies and trying to determine which one they want to work at. A company may be represented by multiple people, and the applicant may be interested in multiple jobs at that one company, but _assume for the sake of the exercise that each company has **one open job** and the user is only talking to **one point of contact**_.

The backend of the app should be **Django project**; put your code in `/backend`.

The frontend of the app should be a **React app**; put your code in `/frontend`.

Design and implementation are up to you, but strive for simplicity in both.

The use of external frameworks and packages is totally acceptable and even encouraged when they are industry standard/best practice.


## Data

What you store is up to you. You may decide to store additional data. But to cover the basics, ensure you're tracking:

 * Users
   - Email
   - Name
 * Jobs
   - Job Title
   - Company
   - Interviewer
 * Notes
   - Note text
   - Timestamp

For the sake of simplicity, use SQLite out of the box, as it comes with Django.

Your backend and frontend should communicate in JSON.


## Authentication

Store users, but skip authentication! Call that one a "gimme" for future expansion.


## User Interface

Again, skip login. But provide an experiences to:

 * View jobs you're tracking
 * Take a note while on a call


## Frontend

Your frontend can be either a web app or a React Native app. Most projects are web apps. No consideration is made toward the choice of frontend app in the evaluation of your project; whatever is most comfortable for you.

If you use a module loader, precompiler, or other packaging tools, please make sure you include un-minified sources.

Don't worry too much about how it looks ... but don't worry too little!

### Requirements

 - [ ] List jobs
 - [ ] View notes for jobs
 - [ ] Take a note for a job
 - [ ] Works in latest versions of Chrome, Firefox, Safari and Internet Explorer. No need to support anything older than that.


## Backend

Your backend must be a Django (current version) app with standard Python. You may include any additional layers, if you like, but we assume the backend will be run with `runserver`, so please include instructions to start your server if it is non-obvious.

### Requirements

 - [ ] Return jobs for a given user
 - [ ] Return notes for a given job
 - [ ] Create a note for a given job
 - [ ] JSON back and forth
 - [ ] SQLite database


# Thanks!

Looking forward to checking it out!
